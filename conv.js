const length = document.getElementById('length');
const result = document.getElementById('result');
const uppercase = document.getElementById('uppercase');
const numbers = document.getElementById('numbers');
const symbols = document.getElementById('symbols');


generate.addEventListener('submit', e => {
    e.preventDefault()
    const len = parseint(length.value);
    const upper = uppercase.checked;
    const num = numbers.checked;
    const sym = symbols.checked;
    const password = generatepassword(len,upper,num,sym);
    document.getElementById('result').value = password;
});

const lowercase_char_codes = arrayfromlowtohigh(97,122);
const uppercase_char_codes = arrayfromlowtohigh(65,90);
const number_char_codes = arrayfromlowtohigh(48,57);
const symbol_char_codes = arrayfromlowtohigh(33,47);  

function generatepassword(len,upper,num,sym){
    let charcodes = lowercase_char_codes;
    if(upper) charcodes = charcodes.concat(uppercase_char_codes);
    if(num) charcodes = charcodes.concat(number_char_codes);
    if(sym) charcodes = charcodes.concat(symbol_char_codes);
    const passwordcharacter = [];
    for(let i = 0; i < len; i++){
        const charactercode = charcodes[Math.floor(Math.random() * charcodes.length)];
        passwordcharacter.push(String.fromCharCode(charactercode));
    }
    return passwordcharacter.join('');
};

function arrayfromlowtohigh(low,high){
    const array = [];
    for(let i = low;i<=high;i++){
        array.push(i);
    }
    return array;
};

